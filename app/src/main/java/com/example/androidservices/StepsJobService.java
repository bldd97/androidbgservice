package com.example.androidservices;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class StepsJobService extends android.app.job.JobService {
    @Override
    public boolean onStartJob(JobParameters params) {

        Toast.makeText(this, "Job is running", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
