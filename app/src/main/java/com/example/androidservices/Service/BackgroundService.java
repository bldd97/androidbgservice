package com.example.androidservices.Service;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.widget.Toast;

import com.example.androidservices.StepsJobService;

public class BackgroundService extends Service {

    private static final int STEPS_JOB_ID = 0;

    private SensorManager sensorManager;
    private Sensor stepsSensor;
    private JobScheduler scheduler;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Service Created", Toast.LENGTH_SHORT).show();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        stepsSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        if (stepsSensor == null) {
            Toast.makeText(this, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show();
        } else {
            
            scheduleJob();
        }
    }

    private void scheduleJob() {
        scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

        ComponentName serviceName = new ComponentName(getPackageName(),
                StepsJobService.class.getName());
        JobInfo.Builder builder = new JobInfo.Builder(STEPS_JOB_ID, serviceName);

        //builder.setPeriodic(900000);

        builder.setPeriodic(5000);

        JobInfo myJobInfo = builder.build();
        scheduler.schedule(myJobInfo);

        Toast.makeText(this, "Job Scheduled!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();

        if (scheduler!=null) {
            scheduler.cancelAll();
            scheduler = null;
            Toast.makeText(this, "Jobs cancelled", Toast.LENGTH_SHORT).show();
        }
    }
}
